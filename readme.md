# CC Hangman

CC Hangman Game --- The challenge

## Getting Started

These instructions aims to get you running the application on your local machine for development and testing purposes.
Please feel free to contact, if you have any problem setting up or running the application.

### Prerequisites

* Java 1.8
* Gradle

### Running application

Go to project root, then build project with Gradle

```
$ gradle build
```

What you should see

```
$ gradle run

> Task :run
[main] INFO spark.staticfiles.StaticFilesConfiguration - StaticResourceHandler configured with folder = /public
[Thread-0] INFO org.eclipse.jetty.util.log - Logging initialized @408ms to org.eclipse.jetty.util.log.Slf4jLog
[Thread-0] INFO spark.embeddedserver.jetty.EmbeddedJettyServer - == Spark has ignited ...
[Thread-0] INFO spark.embeddedserver.jetty.EmbeddedJettyServer - >> Listening on 0.0.0.0:8888
[Thread-0] INFO org.eclipse.jetty.server.Server - jetty-9.4.6.v20170531
[Thread-0] INFO org.eclipse.jetty.server.session - DefaultSessionIdManager workerName=node0
[Thread-0] INFO org.eclipse.jetty.server.session - No SessionScavenger set, using defaults
[Thread-0] INFO org.eclipse.jetty.server.session - Scavenging every 660000ms
[Thread-0] INFO org.eclipse.jetty.server.AbstractConnector - Started ServerConnector@7c58346f{HTTP/1.1,[http/1.1]}{0.0.0.0:8888}
[Thread-0] INFO org.eclipse.jetty.server.Server - Started @581ms
<=========----> 75% EXECUTING [53s]
> :run
```

Open browser and point to [http://localhost:8888/](http://localhost:8888/)

## Running the tests

```
$ gradle test
```

## Built With

* [Spark framework](http://sparkjava.com/) - The web framework used
* [Gradle](https://maven.apache.org/) - Dependency Management
* [Vue](https://vuejs.org/) - Front-end 

## Authors

* **Chanun Chirattikanon** - *Initial work* - [chanunc](https://github.com/chanunc), chanun@gmail.com


