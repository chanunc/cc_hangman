package cc.hangman.model;

public class GameMessage {
	
	private String word;
	private String message;
	private int lifeCount;
	private int gameIndex;

	public GameMessage(String message, Game game, int gameIndex) {
		super();
		this.message = message;
		this.word = game != null ? game.getWord() : "";
		this.lifeCount = game != null ? (game.getLimit() - game.getErrors()) : 0;
		this.gameIndex = gameIndex;
	}
	
	public String getWord() {
		return word;
	}

	public void setWord(String word) {
		this.word = word;
	}

	public String getMessage() {
		return message;
	}
	
	public void setMessage(String message) {
		this.message = message;
	}

	public int getLifeCount() {
		return lifeCount;
	}

}
