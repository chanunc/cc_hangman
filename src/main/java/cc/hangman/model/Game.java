package cc.hangman.model;

import static java.lang.Character.toLowerCase;

public class Game {
	private final String correctWord;
	private String word;
	
	private int limit;
	private int errors;
	private boolean finished;
	private String status;

	public Game(String correctWord, int limit) {
		this.correctWord = correctWord;
		this.limit = limit;
		word = correctWord.replaceAll(".", "_");
		this.finished = false;
		this.status = "PROCESSING";
	}

	public int getLimit() {
		return limit;
	}

	public void setLimit(int limit) {
		this.limit = limit;
	}

	public int getErrors() {
		return errors;
	}

	public boolean guessLetter(char letter) {
		boolean guessed = false;

		StringBuilder newWord = new StringBuilder(word.length());

		for (int i = 0; i < correctWord.length(); i++) {
			if (toLowerCase(correctWord.charAt(i)) == toLowerCase(letter)) {
				newWord.append(correctWord.charAt(i));
				guessed = true;
			} else {
				newWord.append(word.charAt(i));
			}
		}

		if (guessed)
			word = newWord.toString();
		else {
			errors++;
			if (isLost()) {
				word = correctWord;
			}
		}
		
		if (isLost() || isWon()) {
			this.finished = true;
			if (isLost()) {
				this.status = "LOST";
			}
			if (isWon()) {
				this.status = "WON";
			}
		}

		return guessed;
	}

	public String getWord() {
		return word;
	}

	public boolean isLost() {
		return errors >= limit;
	}

	public boolean isWon() {
		return errors < limit && word.equals(correctWord);
	}

	public boolean gameFinished() {
		return finished;
	}
	
	public String getStatus() {
		return status;
	}
}