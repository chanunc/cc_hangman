package cc.hangman;

import static spark.Spark.get;
import static spark.Spark.init;
import static spark.Spark.port;
import static spark.Spark.post;
import static spark.Spark.put;
import static spark.Spark.staticFiles;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.Vector;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ThreadLocalRandom;

import org.eclipse.jetty.webapp.Ordering;

import com.google.gson.Gson;

import cc.hangman.model.Game;
import cc.hangman.model.GameMessage;

public class HangmanMain {

	static int GUESS_TIME_LIMIT = 6;
	static String COOKIE_NAME = "CC_HANGMAN";
	static String REQ_ATTRIBUTE_GUESS_LETTER = "letter";
	
	static Map<String, Vector<Game>> gameMap = new ConcurrentHashMap<>();
	
	public static void main(String[] args) {
		
		// Spark configurations
		port(8888);
		staticFiles.location("/public");

		// vender: Google JSON
		Gson gson = new Gson();
		
		// list games
		get("/games", (req, res) -> {
			String userId = req.cookie(COOKIE_NAME);
			if (userId == null) {
				return gson.toJson(null);
			}
			return gson.toJson(gameMap.get(userId));
		});

		
		// start new game
        post("/game", (req, res) -> {
        		// check existing userId in cookie
        		String userId = req.cookies().get(COOKIE_NAME);
        		if(userId == null) {
            		// generate unique userId
            		userId = "user_" + UUID.randomUUID().toString();        			
        		}
        		// set/extend userId to cookie
        		res.cookie(COOKIE_NAME, userId, 60*60*60, false);
        		
        		// Initialise game
        		Game game = new Game(randomWord(), GUESS_TIME_LIMIT);
        		
        		// Save game & user
        		Vector<Game> gameList = gameMap.get(userId) != null ? gameMap.get(userId) : new Vector<Game>();
        		gameList.add(game);
        		gameMap.put(userId, gameList);
        		
        		return gson.toJson(new GameMessage(null, game, gameList.indexOf(game)));
        });
        
        
		// resume game 
		get("/game/:index" ,(req, res) -> {
			String userId = req.cookie(COOKIE_NAME);
			String gameIndex = req.params(":index");
			
			// check new game needed state
			if (userId == null 
					|| !gameMap.containsKey(userId) 
					|| gameMap.get(userId) == null) {
				return gson.toJson(new GameMessage("Please start new game!", null, 0));
			}			
			
			Vector<Game> gameList = gameMap.get(userId);
			
			try {
				// parse index with 0 as default
				int index = Integer.parseInt(gameIndex);

				// get game by index
				Game game = gameList.get(index);

				return gson.toJson(new GameMessage("Game resume...", game, index));
			} catch (IndexOutOfBoundsException e) {
				return gson.toJson(new GameMessage("Game not found", null, 0));
			}
		});
        
		
        // Guess letter
        put("/game/:index/guess", (req, res) -> {
        		// retrieve guess character from request URL
        		char letter = req.queryParams(REQ_ATTRIBUTE_GUESS_LETTER).charAt(0);
        		
        		// get userId
        		String userId = req.cookie(COOKIE_NAME);
    			String gameIndex = req.params(":index");
        		
        		if(userId == null 
        				|| !gameMap.containsKey(userId) 
        				|| gameMap.get(userId) == null) {
        			return gson.toJson(new GameMessage("Please start new game!", null, 0));
        		}
        		
        		Vector<Game> gameList = gameMap.get(userId);
    			try {
    				// parse index with 0 as default
    				int index = Integer.parseInt(gameIndex);

    				// get game by index
    				Game game = gameList.get(index);

    				// put guessing letter to the game
            		game.guessLetter(letter);
            		
            		if(game.isWon()) {
                		return gson.toJson(new GameMessage("You Win!", game, index));        			
            		}
            		if(game.isLost()) {
                		return gson.toJson(new GameMessage("You Lost!", game, index));        			
            		}
            		return gson.toJson(new GameMessage("Keep going you can do it!", game, index));
    			} catch (IndexOutOfBoundsException e) {
    				return gson.toJson(new GameMessage("Game not found", null, 0));
    			}
        });
        
		init();
	}

	// Random get word from collections
	public static String randomWord() {
		List<String> wordCollection = new ArrayList<String>();
		// Set of words
		wordCollection.add("awesome");
		wordCollection.add("impressive");
		wordCollection.add("wonderful");
		wordCollection.add("incredible");
		wordCollection.add("amazing");
		wordCollection.add("stunning");
		wordCollection.add("astounding");
		
		int randomNum = ThreadLocalRandom.current().nextInt(0, wordCollection.size());
		return wordCollection.get(randomNum);
	}
}
