package cc.hangman;

import org.junit.Test;

import cc.hangman.model.Game;

import static org.junit.Assert.*;

public class HangmanTest {
	@Test
	  public void duplicateLettersShouldBeGuessedAtOnce() {
	    Game game = new Game("awesome",6);
	    game.guessLetter('o');
	    assertEquals("____o__", game.getWord());
	  }

	  @Test
	  public void guessLetterIsCaseInsensitive() {
	    Game game = new Game("AweSOME",6);
	    game.guessLetter('A');
	    assertEquals("A______", game.getWord());
	  }

	  @Test
	  public void correctWordIsShownAfterGameOver() {
	    Game game = new Game("sofa",6);
	    game.guessLetter('b');
	    game.guessLetter('c');
	    game.guessLetter('d');
	    game.guessLetter('e');
	    game.guessLetter('g');
	    game.guessLetter('h');
	    game.guessLetter('i');
	    assertTrue(game.isLost());
	    assertEquals("sofa", game.getWord());
	  }
}
