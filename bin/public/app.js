// Constant
const COOKIE_NAME = "CC_HANGMAN";

// Server URL
var apiBaseURL = "http://localhost:8888";

// VUE application
new Vue({
    el: '#game',

    data: {
    	game_message: {
    		message: 'Welcome! Please click start to begin!',
    		word: 'cchangman!',
    		finished: false,
    		lifeCount: 0,
    		gameIndex: 0,
    	},
        letter: {
            char: null
        },
        game: {lifeCount: null},
        games: null,
    },

    methods: {
    	reloadList() {
    		var self = this;	// binding vue data
    		axios.get(apiBaseURL + '/games', {})
    		    .then(function(response) {
    		        var games = response.data;
    		        if (games !== null) {
	    		        // indexing game items
	    		        for (var i = 0; i < games.length; i++) {
	    		        	games[i].index = i;
	    		        	games[i].lifeCount = games[i].limit - games[i].errors;
	    		        }	
    		        }
    		        self.games = games;
    		    })
    		    .catch(function(error) {
    		        apiErrorHandler(error);
    		    });
    	},

    	// start game handler
        startGame() {
            var self = this;	// binding vue data
            axios.post(apiBaseURL + '/game', {})
                .then(function(response) {
                	//console.log(response.data); //DEBUG
                    self.game_message = response.data;

                    self.reloadList();
                })
                .catch(function(error) {
                    apiErrorHandler(error);
                });
        },

        // guess letter handler
        guessLetter(letter) {
            var self = this;	// binding vue data
            axios.put(apiBaseURL + '/game/'+ self.game_message.gameIndex +'/guess?letter=' + self.letter.char, {})
                .then(function(response) {
                	//console.log(response.data); //DEBUG
                	self.game_message = response.data;

                	self.reloadList();
                })
                .catch(function(error) {
                    apiErrorHandler(error);
                });
        },

        // resume game handler
        resumeGame(gameIndex) {
        	var self = this;	// binding vue data
        	//console.log(Cookies.get(COOKIE_NAME)); //DEBUG

            axios.get(apiBaseURL + '/game/'+ gameIndex)
                .then(function(response) {
                    //console.log(response.data); //DEBUG
                    self.game_message = response.data;
                })
                .catch(function(error) {
                    apiErrorHandler(error);
                });
        }
    },
    // On page load event
    beforeMount() {
    	// load default first game
        // this.resumeGame(0);
        this.reloadList();
    }
});




// Common function alert error
function apiErrorHandler(error) {
	console.log(error);

	if (error.request) {
		alert("Server error!");
	}
}